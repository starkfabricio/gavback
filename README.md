## Descri��o

Backend do Teste da GAV feito em Nest (Typescript, Node e Graphql)

## Instala��o

```bash
$ npm install
```

```mysql
crie uma base de dados e configure os acessos no arquivo -> ormconfig.json
```

## Executando o App
$ npm run start


## Graphql
Fa�a as queries na rota:   http://localhost:3000/graphql


