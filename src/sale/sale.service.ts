import { Component } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Sale } from './sale.entity';

@Component()
export class SaleService {
    constructor(
        @InjectRepository(Sale)
        private readonly saleRepository: Repository<Sale>,
    ){ }

    async find(): Promise<Sale[]> {
        return await this.saleRepository.find();
    }

    async findOneById(id: number): Promise<Sale> {
        return await this.saleRepository.findOne(id);
    }

    async create(sale: Sale): Promise<Sale> {
        return await this.saleRepository.save(sale);
    }

    async update(id: number, sale: Sale): Promise<Sale> {

        await this.saleRepository.update(id, sale);
        return await this.saleRepository.findOne(id);
    }

    async delete(id: number): Promise<Sale> {
        const s = await this.saleRepository.findOne(id);
        await this.saleRepository.delete(id);
        return s;
    }

}