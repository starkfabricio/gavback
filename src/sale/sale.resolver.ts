import { Component, UseGuards } from '@nestjs/common';
import { Query, Mutation, Resolver, DelegateProperty } from '@nestjs/graphql';

import { SaleService } from './sale.service';
import { Sale } from './sale.entity';

@Resolver('Sale')
export class SaleResolver {
  constructor(private readonly saleService: SaleService) {}

  @Query('sales')
  async index() {
    return await this.saleService.find();
  }

  @Query('sale')
  async get(obj, args, context, info): Promise<Sale> {
    return await this.saleService.findOneById(args.id);
  }

  @Mutation('saleCreate')
  async create(obj, args: Sale, context, info): Promise<Sale> {
    return  await this.saleService.create(args);
  }

  @Mutation('saleUpdate')
  async update(obj, args: Sale, context, info): Promise<Sale> {
    return  await this.saleService.update(args.id, args);
  }

  @Mutation('saleDelete')
  async delete(obj, args: Sale, context, info): Promise<Sale> {
    return  await this.saleService.delete(args.id);
  }

}