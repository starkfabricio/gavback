import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { User } from '../user/user.entity';

@Entity()
export class Sale {

  @PrimaryGeneratedColumn() id: number;
  @ManyToOne(type => User, user => user.sales) user: User;
  @Column('text') products: string;
  @Column('double') total: number;
  @Column('datetime') created: Date;
  @Column('datetime') updated: Date;

}