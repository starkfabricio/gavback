import * as jwt from 'jsonwebtoken';
import { Component, Inject } from '@nestjs/common';

@Component()
export class AuthService {

  constructor( )
  {}

  async createToken() {
    const expiresIn = 60 * 60,
      secretOrKey = 'secret';
    const user = { email: 'jose@gav.com.br' };
    const token = jwt.sign(user, secretOrKey, { expiresIn });
    return {
      expires_in: expiresIn,
      access_token: token,
    };
  }

  async validateUser(signedUser): Promise<boolean> {
    // lógica de validação, tem que criar um ACL aqui
    return true;
  }
}
