import { Module, MiddlewaresConsumer, NestModule, RequestMethod } from '@nestjs/common';
import { graphqlExpress } from 'apollo-server-express';
import { GraphQLModule, GraphQLFactory } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserModule } from './user/user.module';
import { ProductModule } from './product/product.module';
import { SaleModule } from './sale/sale.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [UserModule, ProductModule, SaleModule, AuthModule, GraphQLModule, TypeOrmModule.forRoot()],
})
export class AppModule implements NestModule {
  constructor(
    private readonly graphQLFactory: GraphQLFactory,
  ) { }

  configure(consumer: MiddlewaresConsumer) {
    const schema = this.createSchema();

    consumer
      .apply(graphqlExpress(req => ({ schema, rootValue: req})))
      .forRoutes({ path: '/graphql', method: RequestMethod.ALL });
  }

  createSchema() {
    const typeDefs = this.graphQLFactory.mergeTypesByPaths('./**/*.graphql');
    const schema = this.graphQLFactory.createSchema({ typeDefs });
    return schema;
  }
}
