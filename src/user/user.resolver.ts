import { Component, UseGuards } from '@nestjs/common';
import { Query, Mutation, Resolver, DelegateProperty } from '@nestjs/graphql';

import { UserService } from './user.service';
import { User } from './user.entity';

@Resolver('User')
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Query('users')
  async index() {
    return await this.userService.find();
  }

  @Query('user')
  async get(obj, args, context, info): Promise<User> {
    return await this.userService.findOneById(args.id);
  }

  @Mutation('userCreate')
  async create(obj, args: User, context, info): Promise<User> {
    return  await this.userService.create(args);
  }

  @Mutation('userUpdate')
  async update(obj, args: User, context, info): Promise<User> {
    return  await this.userService.update(args.id, args);
  }

  @Mutation('userDelete')
  async delete(obj, args: User, context, info): Promise<User> {
    return  await this.userService.delete(args.id);
  }

}