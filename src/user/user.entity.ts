import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Product } from '../product/product.entity';
import { Sale } from '../sale/sale.entity';
@Entity()
export class User {

  @PrimaryGeneratedColumn() id: number;
  @Column() name: string;
  @Column() email: string;
  @Column() password: string;
  @Column() role: string;

  @OneToMany(type => Product, product => product.user)
  products: Product[];

  @OneToMany(type => Sale, sale => sale.user)
  sales: Sale[];

}