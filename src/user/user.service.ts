import { Component } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { Product } from './../product/product.entity';
@Component()
export class UserService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
    ) { }

    async find(): Promise<User[]> {
        return await this.userRepository.find({ relations: ['products', 'sales'] });
    }

    async findOneById(id: number): Promise<User> {
        return await this.userRepository.findOne(id, { relations: ['products', 'sales'] });
    }

    async create(user: User): Promise<User> {
        return await this.userRepository.save(user);
    }

    async update(id: number, user: User): Promise<User> {
        await this.userRepository.update(id, user);
        return await this.userRepository.findOne(id);
    }

    async delete(id: number): Promise<User> {
        const u = await this.userRepository.findOne(id);
        await this.userRepository.delete(id);
        return u;
    }

}