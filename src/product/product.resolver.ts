import { Component, UseGuards } from '@nestjs/common';
import { Query, Mutation, Resolver, DelegateProperty } from '@nestjs/graphql';

import { ProductService } from './product.service';
import { Product } from './product.entity';

@Resolver('Product')
export class ProductResolver {
  constructor(private readonly productService: ProductService) {}

  @Query('products')
  async index() {
    return await this.productService.find();
  }

  @Query('product')
  async get(obj, args, context, info): Promise<Product> {
    return await this.productService.findOneById(args.id);
  }

  @Mutation('productCreate')
  async create(obj, args: Product, context, info): Promise<Product> {
    return  await this.productService.create(args);
  }

  @Mutation('productUpdate')
  async update(obj, args: Product, context, info): Promise<Product> {
    return  await this.productService.update(args.id, args);
  }

  @Mutation('productDelete')
  async delete(obj, args: Product, context, info): Promise<Product> {
    return  await this.productService.delete(args.id);
  }

}