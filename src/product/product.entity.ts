import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { User } from '../user/user.entity';
import { Sale } from '../sale/sale.entity';

@Entity()
export class Product {

  @PrimaryGeneratedColumn() id: number;
  @Column() name: string;
  @Column('text') description: string;
  @Column('double') inprice: number;
  @Column() stock: number;
  @Column() photoUrl: string;

  @ManyToOne(type => User, user => user.products)
  user: User;

}