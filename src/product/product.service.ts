import { Component } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './product.entity';

@Component()
export class ProductService {
    constructor(
        @InjectRepository(Product)
        private readonly productRepository: Repository<Product>,
    ){ }

    async find(): Promise<Product[]> {
        return await this.productRepository.find();
    }

    async findOneById(id: number): Promise<Product> {
        return await this.productRepository.findOne(id);
    }

    async create(product: Product): Promise<Product> {
        return await this.productRepository.save(product);
    }

    async update(id: number, product: Product): Promise<Product> {

        await this.productRepository.update(id, product);
        return await this.productRepository.findOne(id);
    }

    async delete(id: number): Promise<Product> {
        const s = await this.productRepository.findOne(id);
        await this.productRepository.delete(id);
        return s;
    }

}